Extensions
=====================================

Use of extensions is optional and must be agreed between parties. Requests with
extensions unknown to the server will be rejected with HTTP 400 bad request.

For more details on extensions check the TRP standard document.
