# IVSM101 is not enough

The IVSM101 spec defines the message format for complying with the Travel Rule.
The ultimate goal is to identify from whom the money came and to whom it is
send.

In the current specification there is defined an Originator in section 6.2. It
consists of two elements, an *originatorPersons* element and an *accountNumber*.
The former pertains to all sorts of personal identifiable data. The latter the
identification of the source of the money.

We argue that having only an _accountNumber_ is not enough.

## Insufficient data

In the traditional world when money is wired the account holder is also the
sender of the money. This is not the case when dealing with crypto currencies.
With custodial services the originator VASP might not assign a Bitcoin address
to each individual. Or it might assign several. The same happens on the
beneficiary side. It is common practice that addresses are reused. 

When receiving a transaction notification from an originator VASP the beneficiary
VASP does not have enough information to comply to the Travel Rule. The beneficiary
VASP can't tell if the address use as a source of the funds belong to the
originator. Likewise the address for the beneficiary isn't exclusively tied to
the beneficiary due to address reuse.

### Ambiguous addresses

The originator address could be an address into which the originator VASP pooled
virtual assets. This can be done for a variaty of reasons like implementation
ease or in an effort to reduce fees paid to the miners.

A beneficiary address could very well be reused by the beneficiary VASP or even 
by the end user.

### Absent amount

While the Travel Rule per se doesn't require the amount to be present it does
require that the information ['remains  with  the  wire
transfer'](http://www.fatf-gafi.org/media/fatf/documents/recommendations/pdfs/FATF%20Recommendations%202012.pdf).
Doing this is impossible given the ambiguous addresses. There must be a strict
one to one correlation between the information submitted to the beneficiary VASP
and the value transfer on the block chain. Please note that having a transaction
hash/ID might not even be sufficient to uniquely identify a transfer of coins on
chain. An address might appear twice in the vout set (however unlikely). It 
seems a transaction ID in combination with a reference to particular vin and 
vout are required. Pointing to a particular vin should be done with the vout
field thereof and pointing to a particular vout should be done with the n field.
