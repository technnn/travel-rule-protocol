// Render by copy pasting this text to https://sequencediagram.org/
title Blackhole scenario

participantgroup #lightgreen **Remitting VASP**
participant Crypto Custody
participant 21Travel A
end
participantgroup #lightblue **Beneficiary\nVASP**
participant 21Travel B
end

Crypto Custody->21Travel A: Complete user\nsupplied data
21Travel A->21Travel B: PUT transaction notification
note over 21Travel A,21Travel B: request contains:\n remitting VASP,\n remitting user,\n beneficiary user 
21Travel B-->21Travel A: 201 OK
