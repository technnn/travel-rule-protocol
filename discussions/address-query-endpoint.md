The address query endpoint is a source of some confusion. Different use cases have emerged from the various discussions. 
Here is an overview of these uses cases.

# Discovery
Given a crypto address, the remitting VASP queries all known other VASPs to discover the beneficiary client information. Once it has this information the transaction notification can be sent to the relevant beneficiary VASP.

Minimal information provided by remitting user:
- crypto address
- amount

Information returned by address query endpoint:
- name, date of birth, etc. of the beneficiary client

# Pre-flight check
Given the complete beneficiary information, the remitting VASP queries the beneficiary VASP for correctness of the user supplied information. Only if the given information and the information on file with the beneficiary VASP match a go ahead response is given.

Minimal information provided by remitting user:
- crypto address
- amount
- name, date of birth, etc. of the beneficiary client
- beneficiary VASP

Information returned by address query endpoint:
- binary 'go/no go'

# Blackhole
Given the complete beneficiary information, dispense with the address query altogether and send the transaction notification. If the sent information does not match the on file information the beneficiary VASP notifies the remitting VASP out of band.

Minimal information provided by remitting user:
- crypto address
- amount
- name, date of birth, etc. of the beneficiary client
- beneficiary VASP

Information returned by address query endpoint:
- N/A

# Address creation
Given the beneficiary information, the remitting VASP asks the beneficiary VASP for a crypto address. It then sends the coins to that address and notifies the beneficiary VASP.

Minimal information provided by remitting user:
- amount
- external ID of the beneficiary client
- beneficiary VASP

Information returned by address query endpoint:
- crypto address
